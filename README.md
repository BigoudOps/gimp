### This is a bundle of brushes and scripts for gimp

[![Join the Discord channel](https://img.shields.io/static/v1.svg?label=%20Rejoignez-moi%20sur%20Discordl&message=%F0%9F%8E%86&color=7289DA&logo=discord&logoColor=white&labelColor=2C2F33)](https://discord.gg/bfB6Ve6) 

![visitors](https://visitor-badge.glitch.me/badge?page_id=BigoudOps.readme)

[![Reddit profile](https://img.shields.io/reddit/subreddit-subscribers/apdm?style=social)](https://www.reddit.com/r/apdm) 

## How to use this stuff

1. Unzip files in your local folder `.config/GIMP/2.10/`
2. PSD files is an example of banner for streaming 
3. untar the file brushes inside `.config/GIMP/2.10/brushes/`with command line like `tar -zxvf brushes.tar.gz`
 
